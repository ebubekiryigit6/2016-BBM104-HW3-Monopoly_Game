import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * GameBoard class which has id,name and action function.
 */
public abstract class GameBoard {
    /**
     * which is in game board square's id
     */
    private int id;
    /**
     * game board squares name
     */
    private String name;
    /**
     * if game board squares is property, it has a cost.
     */
    private int cost;
    /**
     *
     * @param id game board squares id, There is 40 square
     */
    public void setId(int id) {
        this.id = id;
    }
    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    public void setName(String name) {
        this.name = name;
    }
    /**
     *
     * @return square id in game board
     */
    public int getId() {
        return id;
    }
    /**
     *
     * @return square name in game board
     */
    public String getName() {
        return name;
    }
    /**
     *
     * @return if square is a property, cost is it's cost, then return cost
     */
    public int getCost() {return cost;}

    /**
     *
     * @param cost game square property's cost
     */
    public void setCost(int cost) {this.cost = cost;}

    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public GameBoard(int id, String name){
        this.id = id;
        this.name = name;
    }

    /**
     *This function works and objects from the membrane.
     *After a few steps from the players to go forward calculate other function calls.
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException any IO exception? throws it.
     * @throws ParseException any JSON parse exception? throw it.
     */
    public void action(Properties properties,Banker banker, Players players,Players otherPlayer,int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {

    }

    /**
     *
     * @return GameBoard[] object array which stores tax,action,jail,go to jail... All of them has id and name.
     * @throws IOException any IO exception? throws it.
     * @throws ParseException any JSON parse exception? throw it.
     */
    public GameBoard[] getGameBoardArray() throws IOException, ParseException {
        GameBoard[] gameBoards = new GameBoard[41];

        JSONParser parser = new JSONParser();
        Object listJson = parser.parse(new FileReader("property.json"));

        JSONObject jsonObject = (JSONObject) listJson;

        JSONArray lands = (JSONArray) jsonObject.get("1");
        JSONArray railRoads = (JSONArray) jsonObject.get("2");
        JSONArray companies = (JSONArray) jsonObject.get("3");

        for (int i = 0; i < lands.size(); i++) {
            JSONObject item = (JSONObject) lands.get(i);
            Properties tempObject = new Properties(0,"temp1");
            tempObject.setId(Integer.parseInt(item.get("id").toString()));
            tempObject.setName(item.get("name").toString());
            tempObject.setCost(Integer.parseInt(item.get("cost").toString()));

            int tempId = Integer.parseInt(item.get("id").toString());
            gameBoards[tempId] = tempObject;
        }
        for (int j = 0; j < railRoads.size(); j++) {
            JSONObject item = (JSONObject) railRoads.get(j);
            Properties tempObject = new Properties(0,"temp1");
            tempObject.setId(Integer.parseInt(item.get("id").toString()));
            tempObject.setName(item.get("name").toString());
            tempObject.setCost(Integer.parseInt(item.get("cost").toString()));

            int tempId = Integer.parseInt(item.get("id").toString());
            gameBoards[tempId] = tempObject;
        }
        for (int k = 0; k < companies.size(); k++) {
            JSONObject item = (JSONObject) companies.get(k);
            Properties tempObject = new Properties(0,"temp1");
            tempObject.setId(Integer.parseInt(item.get("id").toString()));
            tempObject.setName(item.get("name").toString());
            tempObject.setCost(Integer.parseInt(item.get("cost").toString()));

            int tempId = Integer.parseInt(item.get("id").toString());
            gameBoards[tempId] = tempObject;
        }
        gameBoards[1] = new GoSquare(1,"GO");
        gameBoards[3] = new ChestCards(3,"chest");
        gameBoards[18] = new ChestCards(18,"chest");
        gameBoards[34] = new ChestCards(34,"chest");
        gameBoards[8] = new ChanceCards(8,"chance");
        gameBoards[23] = new ChanceCards(23,"chance");
        gameBoards[37] = new ChanceCards(37,"chance");
        gameBoards[5] = new TaxSquares(5,"tax");
        gameBoards[39] = new TaxSquares(39,"tax");
        gameBoards[21] = new FreeParking(21,"freeParking");
        gameBoards[31] = new GoToJail(31,"goToJail");
        gameBoards[11] = new Jail(11,"jail");

        return gameBoards;
    }
}
