import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Objects;

/**
 * ChestCards class there is 11 chest card.
 */
public class ChestCards extends ActionSquares {
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public ChestCards(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return game board squares id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @return an arraylist which stores chest cards
     * @throws IOException
     * @throws ParseException
     */
    public ArrayList getCommunityChestList() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object listJson = parser.parse(new FileReader("list.json"));

        JSONObject jsonObject = (JSONObject) listJson;

        JSONArray jsonCommunityList = (JSONArray) jsonObject.get("communityChestList");

        ArrayList<Object> communityChestList = new ArrayList<>();
        for (int i = 0; i < jsonCommunityList.size(); i++) {
            JSONObject item = (JSONObject) jsonCommunityList.get(i);
            communityChestList.add(item.get("item"));
        }
        return communityChestList;
    }

    /**
     *
     * Player comes here and draws next card and index increase 1
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param index chest card array index
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    public void makeChest(Properties properties,Banker banker,Players players, Players otherPlayer,int dice,int index, PrintWriter writer) throws IOException, ParseException {
        ArrayList chanceList = getCommunityChestList();
        switch (index) {
            case 0:
                players.setPosition(1);
                players.money += 200;
                banker.money -= 200;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + "1" + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(0).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + "1" + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(0).toString());
                }
                break;
            case 1:
                int endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                 int position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 75;
                banker.money -= 75;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(1).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(1).toString());
                }
                break;
            case 2:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                if (players.money < 50){
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                    Util util = new Util();
                    if (Objects.equals(players.getPlayerId(), "Player 1")){
                        util.show(players,otherPlayer,banker,writer);
                    }
                    else {
                        util.show(otherPlayer,players,banker,writer);
                    }
                    writer.close();
                    System.exit(1);
                }
                players.money -= 50;
                banker.money += 50;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(2).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(2).toString());
                }
                break;
            case 3:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 10;
                otherPlayer.money -= 10;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(3).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(3).toString());
                }
                break;
            case 4:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 50;
                otherPlayer.money -= 50;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(4).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(4).toString());
                }
                break;
            case 5:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 20;
                banker.money -= 20;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(5).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(5).toString());
                }
                break;
            case 6:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 100;
                banker.money -= 100;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(6).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(6).toString());
                }
                break;
            case 7:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                if (players.money < 100){
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                    Util util = new Util();
                    if (Objects.equals(players.getPlayerId(), "Player 1")){
                        util.show(players,otherPlayer,banker,writer);
                    }
                    else {
                        util.show(otherPlayer,players,banker,writer);
                    }
                    writer.close();
                    System.exit(1);
                }
                players.money -= 100;
                banker.money += 100;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(7).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(7).toString());
                }
                break;
            case 8:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                if (players.money < 50){
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                    Util util = new Util();
                    if (Objects.equals(players.getPlayerId(), "Player 1")){
                        util.show(players,otherPlayer,banker,writer);
                    }
                    else {
                        util.show(otherPlayer,players,banker,writer);
                    }
                    writer.close();
                    System.exit(1);
                }
                players.money -= 50;
                banker.money += 50;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(8).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(8).toString());
                }
                break;
            case 9:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 100;
                banker.money -= 100;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(9).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(9).toString());
                }
                break;
            case 10:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 50;
                banker.money -= 50;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(10).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(10).toString());
                }
                break;
        }
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action (Properties properties, Banker banker, Players players, Players otherPlayer,int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer)throws IOException, ParseException {

        makeChest(properties,banker,players,otherPlayer,dice,chest.index,writer);
        chest.index++;
        if (chest.index > 10){
            chest.index = chest.index % 11;
        }
    }

}
