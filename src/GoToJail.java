import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * GotoJail class, if player is in go to jail square, he goes to jail and waits 3 tour doing nothing.
 */
public class GoToJail extends GameBoard{
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public GoToJail(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return GotoJail square id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return GotoJail square name
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        players.setPosition(11);
        players.inJail = true;

        Jail jail = new Jail(0,"temp");
        jail.action(properties,banker,players,otherPlayer,dice,chance,chest,writer);
    }
}
