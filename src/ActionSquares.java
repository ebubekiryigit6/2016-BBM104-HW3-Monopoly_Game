import org.json.simple.parser.ParseException;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Action squares are ChanceCards and ChestCards, there are six action squares.
 */
public class ActionSquares extends GameBoard{
    /**
     * index is chestList or chanceList's array index
     */
    public int index;

    /**
     * ActionSquares constructor
     * @param id which is in game board square's id
     * @param name game board squares name
     */
    public ActionSquares(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return returns its game board square id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {

    }
}
