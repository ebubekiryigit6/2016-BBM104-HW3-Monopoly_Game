import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * Jail class, if player is in Jail, he waits 3 tour doing nothing.
 */
public class Jail extends GameBoard{
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public Jail(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return game board squares id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return game board squares name (tax,property ex.)
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties, Banker banker, Players players, Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        players.inJail = true;
        if (Objects.equals(players.getPlayerId(), "Player 1")) {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + "11" + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " went to Jail");
        }
        else {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + "11" + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " went to Jail");
        }
    }

    /**
     *
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param banker Banker class' object
     * @param writer PrintWriter class' object
     */
    public void jailAction(Players players,Players otherPlayer,Banker banker,int dice, PrintWriter writer){
        if (Objects.equals(players.getPlayerId(), "Player 1")) {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + "11" + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " in Jail\t" + "(count="+players.jailCount+")");
        }
        else {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + "11" + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " in Jail\t" + "(count="+players.jailCount+")");
        }
    }
}
