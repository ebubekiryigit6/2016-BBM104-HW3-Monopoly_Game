import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * ReadFile class read txt files and returns an array which stores files' lines.
 */
public class ReadFile {
    /**
     *
     * @param path where is file path
     * @return an array which stores files' lines.
     */
    public String[] readFile(String path) {
        try {
            int i = 0;
            int length = Files.readAllLines(Paths.get(path)).size();
            String[] results = new String[length];
            for (String line : Files.readAllLines(Paths.get(path))) {
                results[i++] = line;
            }
            return results;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     *
     * @param args file arguments
     * @return an array which stores command.txt argument lines.
     */
    public String[] readCommand(String args){
        return readFile(args);
    }
}
