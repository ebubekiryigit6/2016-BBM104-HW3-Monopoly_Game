import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Player 2 class
 */
public class Player2 extends Players{

    public String playerId = "Player 2";

    /**
     *
     * @return an arraylist which stores company properties
     */
    @Override
    public ArrayList<String> getCompanies() {
        return super.getCompanies();
    }

    /**
     *
     * @return an arraylist which stores land properties
     */
    @Override
    public ArrayList<String> getLands() {
        return super.getLands();
    }

    /**
     *
     * @return an arraylist which stores rail road properties
     */
    @Override
    public ArrayList<String> getRailRoads() {
        return super.getRailRoads();
    }

    /**
     *
     * @return steps
     */
    @Override
    public int getI() {
        return super.getI();
    }

    /**
     *
     * @return how many time in jail
     */
    @Override
    public int getJailCount() {
        return super.getJailCount();
    }

    /**
     *
     * @return players position in game board
     */
    @Override
    public int getPosition() {
        return super.getPosition();
    }

    /**
     *
     * @return players name
     */
    @Override
    public String getPlayerId() {
        return playerId;
    }

    /**
     *
     * @return player's money
     */
    public int getMoney() {
        return money;
    }

    /**
     *
     * @param i steps
     */
    @Override
    public void setI(int i) {
        super.setI(i);
    }

    /**
     *
     * @param inJail player is in jail ? True or False
     */
    @Override
    public void setInJail(boolean inJail) {
        super.setInJail(inJail);
    }

    /**
     *
     * @param jailCount player how many times in jail.
     *
     */
    @Override
    public void setJailCount(int jailCount) {
        super.setJailCount(jailCount);
    }

    /**
     *
     * @param playerId players id (Player 1 or Player 2)
     */
    @Override
    public void setPlayerId(String playerId) {
        super.setPlayerId(playerId);
    }

    /**
     *
     * @param position player's position in 40 square
     */
    @Override
    public void setPosition(int position) {
        super.setPosition(position);
    }

    /**
     *
     * @param money player's money
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker, Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        GameBoard[] gameBoards = properties.getGameBoardArray();
        int position = (players.getPosition() + dice) % 40;
        if (position == 0){
            position = 40;
        }
        gameBoards[position].action(properties,banker,players,otherPlayer,dice,chance,chest,writer);
    }


}
