import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * FreeParking class, if player is in free parking square, he waits one tour doing nothing
 */
public class FreeParking extends GameBoard {
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public FreeParking(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return game board squares id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return game board squares name (tax,property ex.)
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        int endPosition = players.getPosition() + dice;
        if (endPosition >= 40){
            players.money += 200;
            banker.money -= 200;
        }
        int position = (endPosition) % 40;
        if (position == 0){
            position = 40;
        }

        if (Objects.equals(players.getPlayerId(), "Player 1")) {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " is in Free Parking ");
        }
        else {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " is in Free Parking ");
        }
        players.setPosition(position);
    }
}
