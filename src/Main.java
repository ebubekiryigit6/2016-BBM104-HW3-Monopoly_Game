import org.json.simple.parser.ParseException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * Main class, this class calls all of the class and function
 */
public class Main {
    /**
     *
     * @param args command.txt argument
     * @throws IOException
     * @throws ParseException
     */
    public static void main(String[] args) throws IOException, ParseException {
/**
 * All the things we use
 */

        ReadFile readFile = new ReadFile();
        String[] commands = readFile.readCommand(args[0]);
        Player1 player1 = new Player1();
        Player2 player2 = new Player2();
        Banker banker = new Banker(0,"temp");
        Jail jail = new Jail(0,"temp");
        Properties properties = new Properties(0,"temp");
        properties.setProperties(properties.getPropertiesArray());
        Util util = new Util();
        ChanceCards chanceCard = new ChanceCards(0,"temp");
        ChestCards chestCard = new ChestCards(0,"temp");
        PrintWriter writer = new PrintWriter(new FileOutputStream("output.txt"));

        for (int i = 0; i < commands.length ; i++) {
            String[] splitArray = commands[i].split(";");
            if (Objects.equals(commands[i], "show()")){
                util.show(player1,player2,banker,writer);
            }
            else if (Objects.equals(splitArray[0], "Player 1")){
                if (player1.inFreeParking){
                    writer.println(player1.getPlayerId() + " is in Free Parking");
                    player1.inFreeParking = false;
                }
                else if (player1.inJail) {
                    player1.jailCount++;
                    jail.jailAction(player1,player2,banker,Integer.parseInt(splitArray[1]),writer);
                    if (player1.jailCount == 3) {
                        player1.inJail = false;
                        player1.jailCount = 0;
                    }
                }
                else {
                    player1.action(properties, banker, player1, player2, Integer.parseInt(splitArray[1]),chanceCard,chestCard,writer);
                }
            }
            else{
                if (player2.inFreeParking){
                    writer.println(player2.getPlayerId() + " is in Free Parking");
                    player2.inFreeParking = false;
                }
                else if (player2.inJail) {
                    player2.jailCount++;
                    jail.jailAction(player2,player1,banker,Integer.parseInt(splitArray[1]),writer);
                    if (player2.jailCount == 3) {
                        player2.inJail = false;
                        player2.jailCount = 0;
                    }
                }
                else {
                    player2.action(properties, banker, player2, player1, Integer.parseInt(splitArray[1]),chanceCard,chestCard,writer);
                }
            }
        }
        util.show(player1,player2,banker,writer);
        writer.close();
    }
}
