import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * Land class is a property
 */
public class Lands extends Properties{
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public Lands(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return land's cost
     */
    @Override
    public int getCost() {
        return super.getCost();
    }

    /**
     *
     * @return land's id in game board square
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return land's name
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @return land's owner player's name
     */
    @Override
    public String getOwner() {
        return super.getOwner();
    }

    /**
     *
     * @param cost game square property's cost
     */
    @Override
    public void setCost(int cost) {
        super.setCost(cost);
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param owned set this owned, true or false
     */
    @Override
    public void setOwned(boolean owned) {
        super.setOwned(owned);
    }

    /**
     *
     * @param owner player1 or player2
     */
    @Override
    public void setOwner(String owner) {
        super.setOwner(owner);
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {

    }
}
