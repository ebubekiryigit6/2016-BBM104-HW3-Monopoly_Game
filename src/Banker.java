import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
/**
 * Banker class, there is one banker and banker's money is 100.000
 */
public class Banker extends GameBoard {
    /**
     * Banker has 100.000 TL
     */
    public int money = 100000;
    /**
     * Properties[] object array which has all of lands,rail roads and companies. Object has id,name,cost.
     */
    public Properties[] properties;

    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public Banker(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     * sets banker's money.
     * @param money banker's money
     */
    public void setMoney(int money) {
        this.money = money;
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @return banker id can be every integer. There is one banker.
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return banker's money
     */
    public int getMoney() {
        return money;
    }

    /**
     *
     * @return banker's name ("banker")
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @return Properties[] object array
     */
    public Properties[] getProperties() {
        return properties;
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {

    }
}
