import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Objects;

/**
 * ChanceCards class, There is six chance card.
 */
public class ChanceCards extends ActionSquares {
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public ChanceCards(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @return game board squares id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return an arraylist which stores chance cards
     * @throws IOException
     * @throws ParseException
     */
    public ArrayList getChanceList() throws IOException, ParseException {
        JSONParser parser = new JSONParser();
        Object listJson = parser.parse(new FileReader("list.json"));

        JSONObject jsonObject = (JSONObject) listJson;

        JSONArray jsonChanceList = (JSONArray) jsonObject.get("chanceList");

        ArrayList<Object> chanceList = new ArrayList<>();
        for (int i = 0; i < jsonChanceList.size(); i++) {
            JSONObject item = (JSONObject) jsonChanceList.get(i);
            chanceList.add(item.get("item"));
        }
        return chanceList;
    }

    /**
     *
     * Player comes here and draws next card and index increase 1
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param index chance array' index
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    public void makeChance(Properties properties,Banker banker,Players players, Players otherPlayer,int dice,int index,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        ArrayList chanceList = getChanceList();
        switch (index){
            case 0:
                players.setPosition(1);
                players.money += 200;
                banker.money -= 200;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + "1" + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(0).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + "1" + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(0).toString());
                }
                break;

            case 1:
                if (!(properties.getProperties()[27].isOwned)) {
                    if (players.money < properties.getProperties()[27].getCost()){
                        writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                        Util util = new Util();
                        if (Objects.equals(players.getPlayerId(), "Player 1")){
                            util.show(players,otherPlayer,banker,writer);
                        }
                        else {
                            util.show(otherPlayer,players,banker,writer);
                        }
                        writer.close();
                        System.exit(1);
                    }
                    players.money -= properties.getProperties()[27].getCost();
                    if (Objects.equals(properties.getProperties()[27].getType(), "land")){
                        String name = properties.getProperties()[27].getName();
                        players.getLands().add(name);
                    }
                    else if(Objects.equals(properties.getProperties()[27].getType(), "railRoad")){
                        players.railRoads.add(properties.getProperties()[27].getName());
                    }
                    else {
                        players.companies.add(properties.getProperties()[27].getName());
                    }
                    if (players.getPosition() > 27) {
                        players.money += 200;
                        banker.money -= 200;
                    }
                    banker.money += properties.getProperties()[27].getCost();
                    properties.getProperties()[27].isOwned = true;
                    properties.getProperties()[27].setOwner(players.getPlayerId());
                    if (Objects.equals(players.getPlayerId(), "Player 1")) {
                        writer.println(players.getPlayerId() + "\t" + dice + "\t" + "27" + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(1).toString() + players.getPlayerId() + " bought " + properties.getProperties()[27].getName());
                    }
                    else {
                        writer.println(players.getPlayerId() + "\t" + dice + "\t" + "27" + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(1).toString() + players.getPlayerId() + " bought " + properties.getProperties()[27].getName());
                    }
                }
                else {
                    if (!Objects.equals(properties.getProperties()[27].getOwner(), players.getPlayerId())) {
                        int rentCost = properties.rentCalculater(properties.getProperties()[27], otherPlayer, dice);
                        if (players.money < rentCost){
                            writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                            Util util = new Util();
                            if (Objects.equals(players.getPlayerId(), "Player 1")){
                                util.show(players,otherPlayer,banker,writer);
                            }
                            else {
                                util.show(otherPlayer,players,banker,writer);
                            }
                            writer.close();
                            System.exit(1);
                        }
                        players.money -= rentCost;
                        otherPlayer.money += rentCost;
                        if (Objects.equals(players.getPlayerId(), "Player 1")) {
                            writer.println(players.getPlayerId() + "\t" + dice + "\t" + "27" + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(1).toString() + players.getPlayerId() + " paid rent for " + properties.getProperties()[27].getName());
                        } else {
                            writer.println(players.getPlayerId() + "\t" + dice + "\t" +"27" + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(1).toString() + players.getPlayerId() + " paid rent for " + properties.getProperties()[27].getName());
                        }
                    }
                }
                players.setPosition(27);
                break;
            case 2:
                int position = (players.getPosition()+ dice - 3) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                GameBoard[] gameBoards = properties.getGameBoardArray();
                if (gameBoards[position] instanceof ChestCards || gameBoards[position] instanceof Properties){
                    dice = 0;
                }
                gameBoards[position].action(properties,banker,players,otherPlayer,dice,chance, chest,writer);
                break;
            case 3:
                int endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                if (players.money < 15){
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                    Util util = new Util();
                    if (Objects.equals(players.getPlayerId(), "Player 1")){
                        util.show(players,otherPlayer,banker,writer);
                    }
                    else {
                        util.show(otherPlayer,players,banker,writer);
                    }
                    writer.close();
                    System.exit(1);
                }
                players.money -= 15;
                banker.money += 15;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(3).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(3).toString());
                }
                break;
            case 4:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 150;
                banker.money -= 150;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(4).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(4).toString());
                }
                break;
            case 5:
                endPosition = players.getPosition() + dice;
                if (endPosition >= 40){
                    players.money += 200;
                    banker.money -= 200;
                }
                position = (endPosition) % 40;
                if (position == 0){
                    position = 40;
                }
                players.setPosition(position);
                players.money += 100;
                banker.money -= 100;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(5).toString());
                }
                else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " draw " + chanceList.get(5).toString());
                }
                break;
        }

    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer,int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {

        makeChance(properties,banker,players,otherPlayer,dice,chance.index,chance,chest,writer);
        chance.index++;
        if (chance.index > 5){
            chance.index = chance.index % 6;
        }
    }
}
