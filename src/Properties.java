import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * Properties class which has lands,rail roads and companies. All property have id,name,cost,owner,isowned,type.
 */
public class Properties extends GameBoard {

    private int cost;
    public boolean isOwned = false;
    private String owner;
    private String type;
    public Properties[] properties;

    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public Properties(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return property's cost
     */
    public int getCost() {
        return cost;
    }

    /**
     *
     * @return owner player if he is
     */
    public String getOwner() {
        return owner;
    }

    /**
     *
     * @return game board squares id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return game board square name
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @param cost game square property's cost
     */
    public void setCost(int cost) {
        this.cost = cost;
    }

    /**
     *
     * @param owned set this owned, true or false
     */
    public void setOwned(boolean owned) {
        isOwned = owned;
    }

    /**
     *
     * @param owner player1 or player2
     */
    public void setOwner(String owner) {
        this.owner = owner;
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param properties Properties[] object array
     */
    public void setProperties(Properties[] properties) {
        this.properties = properties;
    }

    /**
     *
     * @return Properties[] object array
     */
    public Properties[] getProperties() {
        return properties;
    }

    /**
     *
     * @param type land, rail road or company
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     *
     * @return property type
     */
    public String getType() {
        return type;
    }

    /**
     *
     * @param properties Properties class' object
     * @param players Player's class' object
     * @param dice players how many steps go forward
     * @return property's rent cost
     */
    public int rentCalculater(Properties properties,Players players,int dice){
        int cost ;
        if (Objects.equals(properties.getType(), "land")) {
            if (properties.getCost() > 0 && properties.getCost() <= 2000) {
                cost = properties.getCost() / 100 * 40;
            } else if (properties.getCost() > 2000 && properties.getCost() <= 3000) {
                cost = properties.getCost() / 100 * 30;
            } else {
                cost = properties.getCost() / 100 * 35;
            }
        }
        else if (Objects.equals(properties.getType(), "railRoad")){
            cost = players.getRailRoads().size() * 25;
        }
        else {
            cost = 4 * dice;
        }
        return cost;
    }

    /**
     *
     * @return Properties[] object array
     * @throws IOException
     * @throws ParseException
     */
    public Properties[] getPropertiesArray() throws IOException, ParseException {
        Properties[] properties = new Properties[41];

        JSONParser parser = new JSONParser();
        Object listJson = parser.parse(new FileReader("property.json"));

        JSONObject jsonObject = (JSONObject) listJson;

        JSONArray lands = (JSONArray) jsonObject.get("1");
        JSONArray railRoads = (JSONArray) jsonObject.get("2");
        JSONArray companies = (JSONArray) jsonObject.get("3");

        for (int i = 0; i < lands.size(); i++) {
            JSONObject item = (JSONObject) lands.get(i);
            Properties tempObject = new Properties(0,"temp");
            tempObject.setId(Integer.parseInt(item.get("id").toString()));
            tempObject.setName(item.get("name").toString());
            tempObject.setCost(Integer.parseInt(item.get("cost").toString()));
            tempObject.setType("land");

            int tempId = Integer.parseInt(item.get("id").toString());
            properties[tempId] = tempObject;
        }
        for (int j = 0; j < railRoads.size(); j++) {
            JSONObject item = (JSONObject) railRoads.get(j);
            Properties tempObject = new Properties(0,"temp");
            tempObject.setId(Integer.parseInt(item.get("id").toString()));
            tempObject.setName(item.get("name").toString());
            tempObject.setCost(Integer.parseInt(item.get("cost").toString()));
            tempObject.setType("railRoad");

            int tempId = Integer.parseInt(item.get("id").toString());
            properties[tempId] = tempObject;
        }
        for (int k = 0; k < companies.size(); k++) {
            JSONObject item = (JSONObject) companies.get(k);
            Properties tempObject = new Properties(0,"temp");
            tempObject.setId(Integer.parseInt(item.get("id").toString()));
            tempObject.setName(item.get("name").toString());
            tempObject.setCost(Integer.parseInt(item.get("cost").toString()));
            tempObject.setType("company");

            int tempId = Integer.parseInt(item.get("id").toString());
            properties[tempId] = tempObject;
        }
        return properties;

    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' objcect
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer,int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {

        int endPosition = players.getPosition() + dice;
        if (endPosition >= 40){
            players.money += 200;
            banker.money -= 200;
        }
        int position = (endPosition) % 40;
        if (position == 0){
            position = 40;
        }

        if (!(properties.getProperties()[position].isOwned)) {
            if (players.money < properties.getProperties()[position].getCost()){
                writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                Util util = new Util();
                if (Objects.equals(players.getPlayerId(), "Player 1")){
                    util.show(players,otherPlayer,banker,writer);
                }
                else {
                    util.show(otherPlayer,players,banker,writer);
                }
                writer.close();
                System.exit(1);
            }
            players.money -= properties.getProperties()[position].getCost();
            if (Objects.equals(properties.getProperties()[position].getType(), "land")){
                String name = properties.getProperties()[position].getName();
                players.getLands().add(name);
            }
            else if(Objects.equals(properties.getProperties()[position].getType(), "railRoad")){
                players.railRoads.add(properties.getProperties()[position].getName());
            }
            else {
                players.companies.add(properties.getProperties()[position].getName());
            }
            banker.money += properties.getProperties()[position].getCost();
            properties.getProperties()[position].isOwned = true;
            properties.getProperties()[position].owner = players.getPlayerId();
            if (Objects.equals(players.getPlayerId(), "Player 1")) {
                writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " bought " + properties.getProperties()[position].getName());
            }
            else {
                writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " bought " + properties.getProperties()[position].getName());
            }
        }
        else {
            if (!Objects.equals(properties.getProperties()[position].getOwner(), players.getPlayerId())) {
                int rentCost = rentCalculater(properties.getProperties()[position], otherPlayer, dice);
                if (players.money < rentCost){
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
                    Util util = new Util();
                    if (Objects.equals(players.getPlayerId(), "Player 1")){
                        util.show(players,otherPlayer,banker,writer);
                    }
                    else {
                        util.show(otherPlayer,players,banker,writer);
                    }
                    writer.close();
                    System.exit(1);
                }
                players.money -= rentCost;
                otherPlayer.money += rentCost;
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " paid rent for " + properties.getProperties()[position].getName());
                } else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " paid rent for " + properties.getProperties()[position].getName());
                }
            }
            else{
                if (Objects.equals(players.getPlayerId(), "Player 1")) {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " has " + properties.getProperties()[position].getName());
                } else {
                    writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " has " + properties.getProperties()[position].getName());
                }
            }
        }
        players.setPosition(position);

    }
}
