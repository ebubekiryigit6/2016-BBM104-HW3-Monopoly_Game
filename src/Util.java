import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Util class stores only show method.
 */
public class Util {
    /**
     *
     * If there is a show() line in command.txt this function runs and writes:
     *          Player1 "money"   have: "properties"
     *          Player2 "money"   have: "properties"
     *          Banker  "money"
     *          Winner   Player 1 or 2
     *
     * @param player1 player1 object
     * @param player2 player2 object
     * @param banker banker object
     * @param writer PrintWriter object
     */
    public void show(Players player1, Players player2,Banker banker, PrintWriter writer){

        ArrayList<String> player1Properties = new ArrayList<>();
        ArrayList<String> player2Properties = new ArrayList<>();
        player1Properties.addAll(player1.getLands());
        player2Properties.addAll(player2.getLands());
        player1Properties.addAll(player1.getCompanies());
        player2Properties.addAll(player2.getCompanies());
        player1Properties.addAll(player1.getRailRoads());
        player2Properties.addAll(player2.getRailRoads());
        String one = "";
        String two = "";
        for (int i = 0; i < player1Properties.size(); i++) {
            if ( i == player1Properties.size() - 1){
                one = one + player1Properties.get(i);
            }
            else {
                one = one + player1Properties.get(i) + ", ";
            }
        }
        for (int j = 0; j < player2Properties.size(); j++) {
            if ( j == player2Properties.size() - 1){
                two = two + player2Properties.get(j);
            }
            else {
                two = two + player2Properties.get(j) + ", ";
            }
        }

        for (int j = 0; j < 150; j++) {
            writer.print("-");
        }
        writer.println();
        writer.println("Player 1\t" + player1.money + "\t" + "have: " + one);
        writer.println("Player 2\t" + player2.money + "\t" + "have: " + two);
        writer.println("Banker " + banker.getMoney());
        if (player1.money > player2.money) {
            writer.println("Winner " +"Player 1");
        }
        else if (player1.money == player2.money){
            writer.println("Game Draw");
        }
        else {
            writer.println("Winner " +"Player 2");
        }
        for (int j = 0; j < 150 ; j++) {
            writer.print("-");
        }
        writer.println();
    }
}
