import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

/**
 * Players class which has Player 1 and Player 2
 * Every player has position, id, railroadArray, companiesArray, landsArray, inJail, jailCount, money (15.000 TL), inFreeParking
 */
public abstract class Players {

    private int i = 1;
    private int position = i%40;
    private String playerId;
    public ArrayList<String> railRoads = new ArrayList<>();
    public ArrayList<String> companies = new ArrayList<>();
    public ArrayList<String> lands = new ArrayList<>();
    public boolean inJail = false;
    public int jailCount = 0;
    public int money = 15000;
    public boolean inFreeParking = false;

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    public void action(Properties properties, Banker banker, Players players,Players otherPlayer,int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
    }

    /**
     *
     * @return an arraylist which stores company properties
     */
    public ArrayList<String> getCompanies() {
        return companies;
    }

    /**
     *
     * @return an arraylist which stores land properties
     */
    public ArrayList<String> getLands() {
        return lands;
    }

    /**
     *
     * @return an arraylist which stores rail road properties
     */
    public ArrayList<String> getRailRoads() {
        return railRoads;
    }

    /**
     *
     * @return how many steps
     */
    public int getI() {
        return i;
    }

    /**
     *
     * @return how many times in Jail
     */
    public int getJailCount() {
        return jailCount;
    }

    /**
     *
     * @return player's position
     */
    public int getPosition() {
        return position;
    }

    /**
     *
     * @return player's id
     */
    public String getPlayerId() {
        return playerId;
    }

    /**
     *
     * @param i steps
     */
    public void setI(int i) {
        this.i = i;
    }

    /**
     *
     * @param inJail player is in jail ? True or False
     */
    public void setInJail(boolean inJail) {
        this.inJail = inJail;
    }

    /**
     *
     * @param jailCount player how many times in jail.
     *
     * can be up to 3 times
     */
    public void setJailCount(int jailCount) {
        this.jailCount = jailCount;
    }

    /**
     *
     * @param playerId players id (Player 1 or Player 2)
     */
    public void setPlayerId(String playerId) {
        this.playerId = playerId;
    }

    /**
     *
     * @param position player's position in 40 square
     */
    public void setPosition(int position) {
        this.position = position;
    }
}
