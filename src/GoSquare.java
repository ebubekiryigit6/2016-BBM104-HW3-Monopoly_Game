import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * GoSquare class, player is in GoSquare he collects 200TL.
 */
public class GoSquare extends GameBoard {
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public GoSquare(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @return game board squares id
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        players.setPosition(1);
        players.money += 200;
        banker.money -= 200;
        if (Objects.equals(players.getPlayerId(), "Player 1")) {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " is in GO square");
        }
        else {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " is in GO square");
        }
    }
}
