import org.json.simple.parser.ParseException;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Objects;

/**
 * TaxSquares class, if player is in any tax square player pays 100 TL
 */
public class TaxSquares extends GameBoard {
    /**
     *
     * @param id game board squares id, There is 40 square
     * @param name game board squares name (tax,property ex.)
     */
    public TaxSquares(int id, String name) {
        super(id, name);
    }

    /**
     *
     * @return tax square id in game board
     */
    @Override
    public int getId() {
        return super.getId();
    }

    /**
     *
     * @return tax square name "tax"
     */
    @Override
    public String getName() {
        return super.getName();
    }

    /**
     *
     * @param id game board squares id, There is 40 square
     */
    @Override
    public void setId(int id) {
        super.setId(id);
    }

    /**
     *
     * @param name game board squares name (tax,property ex.)
     */
    @Override
    public void setName(String name) {
        super.setName(name);
    }

    /**
     *
     * @param properties Properties class' object
     * @param banker Banker class' object
     * @param players Players class' object
     * @param otherPlayer Players class' object
     * @param dice players how many steps go forward
     * @param chance Chance class' object
     * @param chest Chest class' object
     * @param writer PrintWriter class' object
     * @throws IOException
     * @throws ParseException
     */
    @Override
    public void action(Properties properties,Banker banker,Players players,Players otherPlayer, int dice,ActionSquares chance,ActionSquares chest, PrintWriter writer) throws IOException, ParseException {
        int endPosition = players.getPosition() + dice;
        if (endPosition >= 40){
            players.money += 200;
            banker.money -= 200;
        }
        int position = (endPosition) % 40;
        if (position == 0){
            position = 40;
        }
        if (players.money < 100){
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + players.getPosition() + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " goes bankrupt");
            Util util = new Util();
            if (Objects.equals(players.getPlayerId(), "Player 1")){
                util.show(players,otherPlayer,banker,writer);
            }
            else {
                util.show(otherPlayer,players,banker,writer);
            }
            writer.close();
            System.exit(1);
        }
        players.money -= 100;
        banker.money += 100;
        if (Objects.equals(players.getPlayerId(), "Player 1")) {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + players.money + "\t" + otherPlayer.money + "\t" + players.getPlayerId() + " paid Tax ");
        }
        else {
            writer.println(players.getPlayerId() + "\t" + dice + "\t" + position + "\t" + otherPlayer.money + "\t" + players.money + "\t" + players.getPlayerId() + " paid Tax ");
        }
        players.setPosition(position);
    }
}
